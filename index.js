import * as api from "@actual-app/api";
import { Telegraf, Markup } from "telegraf";
import { message } from "telegraf/filters";

function formatDate(date) {
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, '0'); // Months are zero-indexed
  const day = String(date.getDate()).padStart(2, '0');

  return `${year}-${month}-${day}`;
}

const bot = new Telegraf(process.env.BOT_TOKEN);

bot.start(ctx => ctx.reply('Welcome'));

const states = new Map();

await api.init({
  dataDir: "./data",
  serverURL: process.env.ACTUAL_HOST_PATH,
  password: process.env.ACTUAL_PASSWORD,
});

await api.downloadBudget(process.env.ACTUAL_SYNC_ID);

const accounts = await api.getAccounts();

let categories = await api.getCategories();

categories = categories.filter(c => !c.is_income);

const getCategoryIdByName = name => {
  const category = categories.filter(c => c.name === name);

  if (category.length == 0) {
    return "";
  }

  return category[0].id;
}

console.log({ accounts, categories });

bot.command("track", async ctx => {
  const accountOptions = accounts.map(a => a.name);
  const accountKeyboard = Markup.keyboard(accountOptions).resize();
  ctx.reply("Choose an account:", accountKeyboard);
});

for (const account of accounts) {
  bot.hears(account.name, ctx => {
    states.set(ctx.from.id, { account: { id: account.id, name: account.name } });

    ctx.reply(`You selected ${account.name} now choose the category:`, Markup.keyboard(categories.map(c => c.name)).resize());
  });
}

bot.hears(categories.map(c => c.name), ctx => {
  const userState = states.get(ctx.from.id);

  states.set(ctx.from.id, { ...userState, category: { name: ctx.message.text } });

  const selectedCategory = ctx.message.text;

  ctx.reply(`You selected ${selectedCategory} under account ${userState.account.name}, now please input the amount!`);
});

bot.on(message("text"), async ctx => {
  const userState = states.get(ctx.from.id);

  if (userState.account && userState.category && !userState.transaction) {
    const amount = +ctx.message.text * 100;
    const t = {
      account: userState.account.id,
      date: formatDate(new Date()),
      category: getCategoryIdByName(userState.category.name),
      amount: -amount,
    };

    console.log({ t });

    states.set(ctx.from.id, { ...userState, transaction: t });
    ctx.reply(`Amount saved, now please input the notes!`);
  } else if (userState.account && userState.category && userState.transaction) {
    const notes = ctx.message.text;
    const t = { ...userState.transaction, notes };

    const summary = [
      `Account: ${userState.account.name}`,
      `Category: ${userState.category.name}`,
      `Amount: ${userState.transaction.amount / 100}`,
      `Notes: ${notes}`,
    ];

    ctx.reply(summary.join("\n"));

    await api.addTransactions(userState.account.id, [t]);

    //reset user state
    states.delete(ctx.from.id);

    ctx.reply('Submitted! please type /track to start over.');
  }
});

bot.launch();

process.once("SIGINT", () => {
  api.shutdown();
  bot.stop("SIGINT");
});
process.once("SIGTERM", () => {
  api.shutdown();
  bot.stop("SIGTERM");
});
