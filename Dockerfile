# Use smaller Alpine Node.js base image
FROM docker.io/node:18-alpine AS build

WORKDIR /app

# Copy package.json and install dependencies
COPY package*.json ./
RUN npm ci 

# Copy source code
COPY . .

# Metadata labels
LABEL maintainer="jimmyeatcrab@gmail.com"

# Run app
CMD [ "node", "index.js" ]
